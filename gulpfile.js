var gulp = require('./node_modules/gulp');
var browserify = require('./node_modules/gulp-browserify');
var plumber = require('./node_modules/gulp-plumber');
var concat = require('./node_modules/gulp-concat');

gulp.task('browserify', function () {
    gulp.src('src/main.js')
        .pipe(plumber())
        .pipe(browserify({transform: 'reactify', debug: true}))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('public'));
});

gulp.task('default', ['browserify']);

gulp.task('watch', function () {
    gulp.watch('src/**/*.*', ['default']);
});
