module.exports = {
    // constant key isn't abligitory should has the same name as value, but value must be unique
    BUZZWORD: 'BUZZWORD', // first type of data -> user action
    BUZZWORDED: 'BUZZWORDED', // saved buzzword from a server response after successful save
    FETCHED_BUZZWORDS: 'FETCHED_BUZZWORDS',

    FETCHED_CURRENT_USER: 'FETCHED_CURRENT_USER',
    FETCHED_USERS: 'FETCHED_USERS',

    FOLLOW: 'FOLLOW', // user action
    FOLLOWED: 'FOLLOWED', // saved from server

    UNFOLLOW: 'UNFOLLOW', // user action
    UNFOLLOWED: 'UNFOLLOWED' // saved from server
};

// --- The benefit is to use constants.BUZZWORD instead of just string "BUZZWORD". In one case if we misstyped will be an error in another won't. Also it's recommended convention.
