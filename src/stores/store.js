var assign = require('object-assign');
var EventEmitterProto = require('events').EventEmitter.prototype;
var CHANGE_EVENT = 'CHANGE';

var storeMethods = {
    init: function () {},

    setData: function (models) {
        var currentIds = this._data.map(function (model) {
            return model.cid;
        });

        models.filter(function (model) {
            return currentIds.indexOf(model.cid) === -1;
        }).forEach(this.addData.bind(this));

        console.info('Data setted', this._data);

        this.sortUser();
    },

    addData: function (model) {
        this._data.push(model);
        this.sortUser();
    },

    sortUser: function () {
        this._data.sort(function (a, b) {
            return (+new Date(b.$created)) - (+new Date(a.$created));
        });
    },

    getAllRecords: function () {
        return this._data;
    },

    getRecordById: function (id) {
        return this._data.filter(function (record) {
            return record.cid === id;
        })[0];
    },

    addChangeListener: function (fn) {
        this.on(CHANGE_EVENT, fn);
    },

    removeChangeListener: function (fn) {
        this.removeListener(CHANGE_EVENT, fn);
    },

    emitChangeEvent: function () {
        this.emit(CHANGE_EVENT);
    },

    bind: function (actionType, actionFn) {
        if (this.actions[actionType]) {
            this.actions[actionType].push(actionFn);
        } else {
            this.actions[actionType] = [actionFn];
        }
    }

};

exports.extend = function (methods) {
    var store = {
        _data: [],
        actions: {}
    };

    // extend our store object
    assign(store, EventEmitterProto, storeMethods, methods);

    store.init();

    require('../dispatcher').register(function (action) {
    	if (store.actions[action.actionType]) {
    		store.actions[action.actionType].forEach(function (fn) {
    			fn.call(store, action.data);
    			store.emitChangeEvent();
    		});
    	}
    });

    return store;
};
