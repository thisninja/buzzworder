var constants = require('../constants');

var UserStore = module.exports = require('./store').extend({
    init: function () {
        this.bind(constants.FETCHED_USERS, this.setData);
        this.bind(constants.FOLLOWED, this.updateUser);
        this.bind(constants.UNFOLLOWED, this.updateUser);
    },

    currentUser: USER,

    updateUser: function (user) {
        this.currentUser = user;
    }
});
