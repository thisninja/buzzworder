var constants = require('../constants');
var UserStore = require('./users');

var BuzzwordStore = module.exports = require('./store').extend({
	init: function() {
		this.bind(constants.FETCHED_BUZZWORDS, this.setData);
		this.bind(constants.BUZZWORDED, this.addData);
	},

	timeline: function () {
        var ids = [UserStore.currentUser.cid].concat(UserStore.currentUser.following);
        return this._data.filter(function (buzzword) {
            return ids.indexOf(buzzword.userId) > -1;
        });
	},

	getById: function (id) {
		return this._data.filter(function (buzzword) {
			return buzzword.userId === id;
		});
	}
});
