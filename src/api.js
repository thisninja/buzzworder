var actions = require('./actions');
var dispatcher = require('./dispatcher');
var constants = require('./constants');

var API = module.exports = {
	fetchBuzzWords: function() {
		get('/api/buzzwords').then(actions.fetchedBuzzwords.bind(actions));
	},

	fetchUsers: function () {
		get('/api/users').then(actions.fetchedUsers.bind(actions));
	},

	startFetchingBuzzwords: function () {
        this.fetchBuzzWords();
        //return setInterval(this.fetchBuzzWords, 1000);
    },

    startFetchingUsers: function (){
        this.fetchUsers();
        //return setInterval(this.fetchUsers, 5000);
    },

	saveBuzzword: function(text) {
		text = text.trim();

		if (text === '') return;

		post('/api/buzzwords', {text: text}).then(actions.buzzworded.bind(actions));
	},

	follow: function (id) {
		post('/api/follow/' + id).then(actions.followed.bind(actions));
	},

	unfollow: function (id) {
		post('/api/unfollow/' + id).then(actions.unfollowed.bind(actions));
	}
};

function get(url) {
	return fetch(url, {
		credentials: 'same-origin'
	}).then(function(res) {
		return res.json();
	});
}

function post(url, body) {
	return fetch(url, {
		method: 'POST',
		credentials: 'include',
		body: JSON.stringify(body || {}),
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json'
		}
	}).then(function(res) {
		return res.json();
	});
}

dispatcher.register(function(action) {
	switch(action.actionType) {
		case constants.BUZZWORD:
			API.saveBuzzword(action.data);
			break;
		case constants.FOLLOW:
			API.follow(action.data);
			break;
		case constants.UNFOLLOW:
			API.unfollow(action.data);
	}
});
