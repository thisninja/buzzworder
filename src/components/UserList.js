var React = require('react');
var UserStore = require('../stores/users');
var actions = require('../actions');
var Link = require('react-router').Link;

var FollowButton = require('./FollowButton');
var BuzzwordBox = require('./BuzzwordBox');

var UserList = module.exports = React.createClass({
    getInitialState: function() {
        return {
            users: UserStore.getAllRecords(),
            user: UserStore.currentUser
        };
    },

    render: function () {
        var users = this.state.users.filter(function (user) {
            return this.state.user.cid !== user.cid;
        }.bind(this)).map(function (user) {
            return <BuzzwordBox key={user.cid} user={user}>
                <FollowButton userId={user.cid} />
            </BuzzwordBox>
        });

        return <ul> {users} </ul>
    },

    componentDidMount: function() {
		UserStore.addChangeListener(this.onChange);
	},

    componentWillUnmount: function() {
		// component will unmount when a user will navigate away from home page. However it's possible to update UserStore after unmount and React will try to do it thus we need to removeListener
		UserStore.removeChangeListener(this.onChange);
    },

	onChange: function() {
		this.setState(this.getInitialState());
	}
});
