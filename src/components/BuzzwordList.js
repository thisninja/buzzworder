var React = require('react');
var moment = require('moment');
var BuzzwordBox = require('./BuzzwordBox');

var UserStore = require('../stores/users');

var BuzzwordList = React.createClass({
	render: function() {
		var items = this.props.buzzwords.map(function(buzzword) {
			return (<BuzzwordBox
				key={buzzword.cid}
 				user={buzzword}
			 	timestamp={moment(buzzword.$created).fromNow()}>
				{buzzword.text}
			</BuzzwordBox>);
			});
		return <ul> {items} </ul>
	}
});

module.exports = BuzzwordList;
