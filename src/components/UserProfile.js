var React = require('react');
var ReactRouter = require('react-router');
var UserStore = require('../stores/users');
var BuzzwordStore = require('../stores/buzzwords');
var utils = require('../utils');
var FollowButton = require('./FollowButton');

var UserProfile = React.createClass({
    mixins: [ReactRouter.State],

    getInitialState: function() {
        var id = parseInt(this.getParams().id, 10);

        return {
            user: UserStore.getRecordById(id) || {},
            buzzwords: BuzzwordStore.getById(id)
        };
    },

    render: function() {
        var buzzwords = this.state.buzzwords.map(function (buzzword) {
            return <li key={buzzword.cid}> {buzzword.text} </li>
        });

        return (
            <div>
                <img className="col s2" src={utils.avatar(this.state.user.email)} />

                <div className="col s10">
                    <h1> {this.state.user.fullname} </h1>
                    <h3 className="timestamp"> @{this.state.user.username} </h3>

                    <p> <FollowButton userId={this.state.user.cid} /> </p>

                    <ul>
                        {buzzwords}
                    </ul>
                </div>
            </div>
        );
    },

    componentDidMount: function() {
		UserStore.addChangeListener(this.onChange);
        BuzzwordStore.addChangeListener(this.onChange);
	},

    componentWillUnmount: function() {
		// component will unmount when a user will navigate away from home page. However it's possible to update UserStore after unmount and React will try to do it thus we need to removeListener
		UserStore.removeChangeListener(this.onChange);
        BuzzwordStore.addChangeListener(this.onChange);
    },

	onChange: function() {
		this.setState(this.getInitialState());
	}
});

module.exports = UserProfile;
