var React = require('react');
var RouteHandler = require('react-router').RouteHandler;

var Navigation = require('./Navigation');

var App = React.createClass({
    render: function () {
        return (
            <div>
                <div className="row">
                    <div className="col s3">
                        <Navigation />
                    </div>
                    <div className="col s9">
                        <RouteHandler />
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = App;
