var React = require('react');
var actions = require('../actions');
var UserStore = require('../stores/users');

var FollowButton = React.createClass({
    getInitialState: function() {
        return {
            id: UserStore.currentUser.cid,
            currentlyFollowing: UserStore.currentUser.following
        };
    },

    render: function () {
        // comporing if user not following itself
        //<span> You cant following yourself, smart ass </span>
        if (this.state.id === this.props.userId) {
            return <span> You cant following yourself, smart ass </span>;
        };

        var text, action;

        if (this.state.currentlyFollowing.indexOf(this.props.userId) !== -1) {
            text = 'Unfollow';
            action = this.unfollow;
        } else {
            text = 'Follow';
            action = this.follow;
        }

        return <a className="waves-effect waves-navy btn" onClick={action}> {text} </a>;
    },

    unfollow: function () {
        actions.unfollow(this.props.userId);
    },

    follow: function () {
        actions.follow(this.props.userId);
    },

    componentDidMount: function() {
		UserStore.addChangeListener(this.onChange);
	},

    componentWillUnmount: function() {
		// component will unmount when a user will navigate away from home page. However it's possible to update UserStore after unmount and React will try to do it thus we need to removeListener
		UserStore.removeChangeListener(this.onChange);
    },

	onChange: function() {
		this.setState(this.getInitialState());
	}
});

module.exports = FollowButton;
