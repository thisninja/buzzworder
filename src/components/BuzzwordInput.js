var React = require('react');

var BuzzwordInput = React.createClass({

	getInitialState: function() {
		return {
			value: ''
		};
	},

	render: function() {
		return (
			<div className="row">
				<div className="col s9">
					<input className="" type="text" placeholder="Buzzword something!" onChange={this.handleChange}
						value={this.state.value}/>
				</div>
				<div className="col">
					<a className="waves-effect waves-light btn" onClick={this.handleClick}><i className="material-icons right">cloud</i>Buzzword</a>
				</div>
			</div>
		);
	},

	handleChange: function(e) {
		this.setState({
			value: e.target.value
		});
	},

	handleClick: function(e) {
		this.props.onSave(this.state.value);

		this.setState({
			value: ''
		});
	}
});

module.exports = BuzzwordInput;

/*
* <BuzzwordInput onSave={...} />
*/