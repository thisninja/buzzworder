var React = require('React');
var BuzzwordInput = require('./BuzzwordInput');
var actions = require('../actions');
var BuzzwordList = require('./BuzzwordList');
var BuzzwordStore = require('../stores/buzzwords');

var Home = React.createClass({
	getInitialState: function() {
		return {
			buzzwords: BuzzwordStore.timeline()
		};
	},

	componentDidMount: function() {
		BuzzwordStore.addChangeListener(this.onChange);
	},

	componentWillUnmount: function() {
		// component will unmount when a user will navigate away from home page. However it's possible to update BuzzwordStore after unmount and React will try to do it thus we need to removeListener
		BuzzwordStore.removeChangeListener(this.onChange);
	},

	onChange: function() {
		this.setState(this.getInitialState());
	},

	render: function() {
		return (
			<div>
				<BuzzwordInput onSave={this.saveBuzzword} />
				<BuzzwordList buzzwords={this.state.buzzwords} />
			</div>
		);
	},

	saveBuzzword: function(text) {
		actions.buzzword(text);
	}
});

module.exports = Home;
