var router = module.exports = require('express').Router();
var login = require('./login');

var db = new (require('locallydb'))('./.data');
var BuzzWords = db.collection('buzzwords');

router.route('/api/buzzwords')
    .all(login.required)
    .get(function (req, res) {
        res.json(BuzzWords.toArray());
    })
    .post(function (req, res) {
        var BuzzWord = req.body;

        BuzzWord.userId = req.user.cid;

        // TODO: TO BE REMOVED
        BuzzWord.username = req.user.username;
        BuzzWord.fullname = req.user.fullname;
        BuzzWord.email = req.user.email;

        var BuzzWordId = BuzzWords.insert(BuzzWord);

        res.json(BuzzWords.get(BuzzWordId));
    });
