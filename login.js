var passport = require('passport');
var LocalStrategy = require('passport-local');

var LocallyDB = require('Locallydb');
var db = new LocallyDB('./.data');
var users = db.collection('users');

var crypto = require('crypto');

function makeHash(password) {
    return crypto.createHash('sha512').update(password).digest('hex');
}

passport.use(new LocalStrategy(function (username, password, done) {
    console.log('user --- ', users.where({ username: username, passwordhash: makeHash(password) }));
    var user = users.where({ username: username, passwordhash: makeHash(password) }).items[0];

    if (user) {
        done(null, user);
    } else {
        done(null, false);
    }
}));

passport.serializeUser(function (user, done) {
    done(null, user.cid);
});

passport.deserializeUser(function (cid, done) {
    done(null, users.get(cid));
});

var router = require('express').Router();
var bodyParser = require('body-parser');

// Login Page
router.use(bodyParser.urlencoded({ extended: true }));
// API
router.use(bodyParser.json());
router.use(require('cookie-parser')());
router.use(require('express-session')({
    secret: 'Cherryade & Outer Space',
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());

router.get('/login', function (req, res) {
    res.render('login');
});

router.post('/signup', function (req, res, next) {
    console.log('signup');
    if (users.where({ username: req.body.username }).items.length === 0) {
        // no user
        var user = {
                fullname: req.body.fullname,
                email: req.body.email,
                username: req.body.username,
                passwordhash: makeHash(req.body.password),
                following: [] || 0
        };

        console.log('user, ', user);

        var userId = users.insert(user);

        req.login(users.get(userId), function (err) {
            if (err) return next(err);

            res.redirect('/');
        });
    } else {
        console.log('there is a user');
        // there is a user
        res.redirect('/login');
    }
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login'
}));

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/login');
});

router.get('/api/users', function (req, res) {
    res.json(users.toArray().map(makeUserSafe));
});

router.post('/api/follow/:id', function (req, res) {
    var id = parseInt(req.params.id, 10);

    if (req.user.following.indexOf(id) < 0) {
        req.user.following.push(id);
        users.update(req.user.cid, req.user);
    }

    res.json(makeUserSafe(req.user));
});

router.post('/api/unfollow/:id', function (req, res) {
    var id = parseInt(req.params.id, 10),
        followedPostion = req.user.following.indexOf(id);

    if (followedPostion > -1) {
        req.user.following.splice(followedPostion, 1);
        users.update(req.user.cid, req.user);
    }

    res.json(makeUserSafe(req.user));
});

function checkIfLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.redirect('/login');
    }
}

function makeUserSafe(user) {
    var safeUser = {},
        safeKeys = ['cid', 'fullname', 'email', 'username', 'following'];

        safeKeys.forEach(function (key) {
            safeUser[key] = user[key];
        });

        return safeUser;
}

exports.routes = router;
exports.required = checkIfLoggedIn;
exports.safe = makeUserSafe;
